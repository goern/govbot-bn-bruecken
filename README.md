# [Offene Daten:Bonn]

Momentan kann der bot nur die Daten der Nord-Brücke abfragen...

# Copyright und Lizenz

Der Text der Lizenz ist in der Datei LICENSE mitgeliefert, der deutsche Text ist unter http://www.gnu.de/documents/gpl-3.0.de.html zu finden.

opendata-bonn
Copyright (C) 2017  Christoph Görn <goern@b4mad.net>

Dieses Programm ist freie Software. Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation veröffentlicht, weitergeben und/oder modifizieren, entweder gemäß Version 3 der Lizenz oder (nach Ihrer Option) jeder späteren Version.

Die Veröffentlichung dieses Programms erfolgt in der Hoffnung, daß es Ihnen von Nutzen sein wird, aber OHNE IRGENDEINE GARANTIE, sogar ohne die implizite Garantie der MARKTREIFE oder der VERWENDBARKEIT FÜR EINEN BESTIMMTEN ZWECK. Details finden Sie in der GNU General Public License.

Sie sollten ein Exemplar der GNU General Public License zusammen mit diesem Programm erhalten haben. Falls nicht, siehe <http://www.gnu.org/licenses/>.