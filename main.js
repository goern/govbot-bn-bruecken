/*
 *   govbot-bn-bruecken
 *   Copyright (C) 2017  Christoph Görn
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

const version = '0.2.0';

let config = {"variables": require('dotenv').config()};

const format = require("string-template");
const shared = require('../../modules/sharedFunctions');

const opendata = require('opendata-bonn');
const moment = require('moment-timezone');
moment.locale('de');
moment.tz.setDefault("Europe/Berlin");

const botName = 'bn-bruecken';
const botRoute = '/' + botName;

const botDialog = [
  function(session, args, next) {
      // TODO: Ich würde vorschlagen noch einen 2. Dialog Schritt einzufügen. Im ersten würde man dann auswählen welche Brücke und im 2. Schritt dann die Ausgabe der Daten
    opendata.GetAktuelleStraßenverkehrslage(opendata.FriedrichEbertBrückeBeideRichtungen)
      .then((geojson) => {
        geojson.features.forEach((element) => {
          if (element.properties.strecke_id == opendata.FriedrichEbertBrückeBonnNachBeuel) {
            let result = {
              text: element.properties.verkehrsstatus,
              auswertezeit: moment.tz(element.properties.auswertezeit, "YYYY-MM-DDThh:mm:ss", "Europe/Berlin")
            };
            console.log(result);

            let answerText = generateAnswerTextFromResult(result);
            session.send(answerText);
          }
        });
        session.endDialog();
      }).catch((err) => {
        session.endDialog("Leider ist ein Fehler aufgetreten. Bitte versuchen Sie es später erneut.");
        console.log(err);
    });
  }
];

function generateAnswerTextFromResult(result) {
  let answerTextRaw = resolveAnswers[shared.randomWithRange(0, resolveAnswers.length)];
  return format(answerTextRaw, {
    value: result.text,
    auswertezeit: result.auswertezeit.fromNow(),
  });
}

const resolveAnswers = [
  "Friedrich-Ebert-Brücke: {value} ({auswertezeit})",
  "{value} ... {auswertezeit}",
  '{auswertezeit}: {value}'
];

module.exports = botDialog
